# Wave Function Collapse

This is a repository for a project in c++ language to make a wave function collapse that take image input, scaled size.

## Example of execution

`cd src`

`g++ -o collapse main.cpp helper/helper.cpp wfc/wfc.cpp -lstdc++ -lpng -lm && clear && ./collapse path/to/your/image.png tilingSize outputGridSize`

## TODO

> Flip image, rotate it to extract more tiles

1. Create the grid and create a gridState that will store which tile has been locked in cell
2. Each cell contains a map<tiles, map<string, int>>
3. Start by picking a random cell and locking it in
4. Propagate neighbour possibilities by updating the cells possible to place to the one that can be placed from the source propagation
