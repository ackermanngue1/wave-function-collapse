#include "helper.hpp"
#include <cstddef>
#include <cstring>
#include <dirent.h>
#include <png++/png.hpp>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

const std::string OUTPUT_FOLDER = "../output/";

void createDirectory(const std::string &path) {
  if (access(path.c_str(), F_OK) != 0) {
    if (mkdir(path.c_str(), 0777) == -1) {
      printf("Error creating directory: %s.\n", path.c_str());
    }
  }
}

void saveTiles(
	       std::vector<png::image<png::rgb_pixel>> &tiles){
  int i = 0;
  for (auto &tileImage : tiles) {
    // Create the output directory if it doesn't already exist
    createDirectory(OUTPUT_FOLDER);
    std::string output = OUTPUT_FOLDER + std::to_string(i) + ".png";
    // Save the tile image to file
    tileImage.write(output);
    i++;
  }
}

void displayOccurrences(
			const std::vector<OccurencesCount>
			&occurrences){
  int i = 0;
  for (const auto &entry : occurrences) {
    printf("Tile %d occured %d times in the base image.\n", i, entry.occurences);
    i++;
  }
}

const char* directionToString(DIRECTION direction) {
  switch (direction) {
  case DIRECTION::LEFT: return "LEFT";
  case DIRECTION::RIGHT: return "RIGHT";
  case DIRECTION::UP: return "UP";
  case DIRECTION::DOWN: return "DOWN";
  default: return "UNKNOWN";
  }
}

void displayAdjacencyRules(
			   std::vector<Rules>
			   &rules) {
  for (int i = 0; i < rules.size(); i++) {
    printf("Tile %d:\n", i);
    for (const auto& placement : rules[i].allowedTiles) {
      printf("  Direction: %s\n", directionToString(placement.direction));
      printf("\tAllowed Tiles:\n");
      for (int j = 0; j < placement.allowedTiles.size(); j++) {
	printf("\t  Tile %d\n", placement.tilesID[j]);
      }
    }
  }
}

int index2dTo1d(int x, int y, int size) { return y * size + x; }

void displayGrid(const std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>>& grid) {
  for (int y = 0; y < grid.size(); y++) {
    for (int x = 0; x < grid.size(); x++) {
      printf("Cell [%d][%d]:\n", y, x);
      const std::vector<png::image<png::rgb_pixel>>& cellTiles = grid[y][x];
      for (int i = 0; i < cellTiles.size(); i++) {
	printf("  Tile %d\n", i);
      }
    }
  }
}
