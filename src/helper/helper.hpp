#ifndef HELPER_H
#define HELPER_H
#include <dirent.h>

#include "../wfc/wfc.hpp"
#include <map>
#include <png++/png.hpp>
#include <string>
#include <vector>

void createDirectory(const std::string &path);
void saveTiles(
	       std::vector<png::image<png::rgb_pixel>> &tiles);

void displayOccurrences(
			const std::vector<OccurencesCount>
			&occurrences);

void displayAdjacencyRules(std::vector<Rules> &rules);

int index2dTo1d(int x, int y, int size);

void displayGrid(const std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>>& grid);

#endif // HELPER_H
