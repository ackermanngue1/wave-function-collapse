#include "helper/helper.hpp"
#include "wfc/wfc.hpp"
#include <iostream>
#include <map>
#include <string>

int main(int argc, char *argv[]) {
  if (argc < 4) {
    std::cerr << "Usage: " << argv[0]
              << " baseImage.png tilingSize outputGridSize" << std::endl;
    return 1;
  }

  std::string baseImageFile = argv[1];
  int tilingSize = atoi(argv[2]);
  int outputGridSize = atoi(argv[3]);

  printf("parameters : %s, %d, %d\n", baseImageFile.c_str(), tilingSize, outputGridSize);
  const png::image<png::rgb_pixel> baseImage(baseImageFile);
  printf("base image (w: %d, h: %d)\n", baseImage.get_width(), baseImage.get_height());
  printf("mid cell (x: %d, y: %d)\n", outputGridSize / 2, outputGridSize / 2);

  std::vector<png::image<png::rgb_pixel>> tiles = extractTiles(baseImage, tilingSize);
  // saveTiles(tiles);

  printf("tiles count : %ld\n", tiles.size());
  printf("output image (w: %d, h: %d)\n", outputGridSize * tilingSize, outputGridSize * tilingSize);

  std::vector<OccurencesCount> occurences;
  // std::vector<OccurencesCount> occurences = countOccurrences(baseImage, tiles);
  // displayOccurrences(occurences);

  printf("Creating rules...\n");

  std::vector<Rules> rules =  generateAdjacencyRules(tiles);
  // displayAdjacencyRules(rules);

  printf("Grid creation...\n");
  
  auto grid = createGrid(outputGridSize, tiles);
  // displayGrid(grid);

  printf("Collapsing...\n");

  auto output = collapse(grid, occurences, rules, tilingSize, tiles);

  return 0;
}
