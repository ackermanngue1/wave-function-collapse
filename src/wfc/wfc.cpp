#include "wfc.hpp"
#include "../helper/helper.hpp"

#include <cstddef>
#include <dirent.h>
#include <list>
#include <map>
#include <png++/rgb_pixel.hpp>
#include <utility>
#include <limits>
#include <random>

std::vector<png::image<png::rgb_pixel>>
extractTiles(const png::image<png::rgb_pixel> &baseImage, int tileSize) {
  std::vector<png::image<png::rgb_pixel>> tiles;

  // Iterate over the base image with overlapping tiles
  for (int y = 0; y < baseImage.get_height(); y++) {
    for (int x = 0; x < baseImage.get_width(); x++) {
      png::image<png::rgb_pixel> tile(tileSize, tileSize);

      // Iterate over the pixels in the tile
      for (int ty = 0; ty < tileSize; ty++) {
        for (int tx = 0; tx < tileSize; tx++) {
          // Calculate the corresponding position in the base image
          int px = (x + tx);
          int py = (y + ty);

          // Ensure that the calculated positions are within bounds
          px = px < baseImage.get_width() ? px : px - baseImage.get_width();
          py = py < baseImage.get_height() ? py : py - baseImage.get_height();

          // Assign the pixel from the base image to the tile
          tile[ty][tx] = baseImage[py][px];
        }
      }

      // Store the tile in the map
      tiles.insert(tiles.end(), tile);
    }
  }

  return tiles;
}

std::vector<OccurencesCount>
countOccurrences(const png::image<png::rgb_pixel> &baseImage,
                 const std::vector<png::image<png::rgb_pixel>> &tiles) {
  std::vector<OccurencesCount> occurences;

  const int width = baseImage.get_width();
  const int height = baseImage.get_height();
  
  for (const png::image<png::rgb_pixel> currentTile : tiles) {
    OccurencesCount currentOccurences = {.tile = currentTile, .occurences = 0};

    const int tileWidth = currentTile.get_width();
    const int tileHeight = currentTile.get_height();

    for (uint32_t y = 0; y < height; y++) {
      for (uint32_t x = 0; x < width; x++) {
	// Check if the current tile matches the sub-image of the base image
        bool match = true;
        for (int ty = 0; ty < tileHeight; ty++) {
          for (int tx = 0; tx < tileWidth; tx++) {
            // Calculate the corresponding position in the base image
            int px = (x + tx);
            int py = (y + ty);

            // Ensure that the calculated positions are within bounds
            px = px < baseImage.get_width() ? px : px - baseImage.get_width();
            py = py < baseImage.get_height() ? py : py - baseImage.get_height();
            auto basePixel = baseImage[py][px];
            auto tilePixel = currentTile[ty][tx];
            if (basePixel.red != tilePixel.red ||
                basePixel.green != tilePixel.green ||
                basePixel.blue != tilePixel.blue) {
              match = false;
              break;
            }
          }
          if (!match) {
            break;
          }
        }

	if (match) {
	  currentOccurences.occurences++;
	}
      }
    }
    occurences.insert(occurences.end(), currentOccurences);
  }
 
  return occurences;
}

bool checkTilesOverlappingCompability(
				      const png::image<png::rgb_pixel> &imageA,
				      const png::image<png::rgb_pixel> &imageB,
				      DIRECTION direction){

  int heightA = imageA.get_height();
  int widthA = imageA.get_width();
  int heightB = imageB.get_height();
  int widthB = imageB.get_width();

  switch (direction) {
  case DIRECTION::DOWN:
    for (int x = 0; x < widthA && x < widthB; x++) {
      auto pixelA = imageA[heightA - 1][x];
      auto pixelB = imageB[0][x];
      if (pixelA.red != pixelB.red || pixelA.green != pixelB.green ||
          pixelA.blue != pixelB.blue) {
        return false;
      }
    }
    return true;
  case DIRECTION::RIGHT:
    for (int y = 0; y < heightA && y < heightB; y++) {
      auto pixelA = imageA[y][widthA - 1];
      auto pixelB = imageB[y][0];
      if (pixelA.red != pixelB.red || pixelA.green != pixelB.green ||
          pixelA.blue != pixelB.blue) {
        return false;
      }
    }
    return true;
  case DIRECTION::UP:
    for (int x = 0; x < widthA && x < widthB; x++) {
      auto pixelA = imageA[0][x];
      auto pixelB = imageB[heightB - 1][x];
      if (pixelA.red != pixelB.red || pixelA.green != pixelB.green ||
          pixelA.blue != pixelB.blue) {
        return false;
      }
    }
    return true;
  case DIRECTION::LEFT:
    for (int y = 0; y < heightA && y < heightB; y++) {
      auto pixelA = imageA[y][0];
      auto pixelB = imageB[y][widthB - 1];
      if (pixelA.red != pixelB.red || pixelA.green != pixelB.green ||
          pixelA.blue != pixelB.blue) {
        return false;
      }
    }
    return true;
  default:
    return false;
  }
}

std::vector<Rules>
generateAdjacencyRules(
		       const std::vector<png::image<png::rgb_pixel>> &tiles
		       ) {
  std::vector<Rules> rules;
  for (const auto &tileA : tiles) {
    Rules rule;
    rule.tile = tileA;
    for (DIRECTION direction : {DIRECTION::LEFT, DIRECTION::RIGHT,
				DIRECTION::UP, DIRECTION::DOWN}) {
      AllowedTilePlacement placement;
      placement.direction = direction;
      int i = 0;
      for (const auto &tileB : tiles) {
	if (checkTilesOverlappingCompability(tileA, tileB, direction)) {
	  placement.allowedTiles.push_back(tileB);
	  placement.tilesID.push_back(i);
	}
	i++;
      }
      if (!placement.allowedTiles.empty()) {
	rule.allowedTiles.push_back(placement);
      }
    }
    if (!rule.allowedTiles.empty()) {
      rules.push_back(rule);
    }
  }
  return rules;
}

std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>>
createGrid(int gridSize,
           std::vector<png::image<png::rgb_pixel>> tiles) {
  std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>> grid(gridSize, std::vector<std::vector<png::image<png::rgb_pixel>>>(gridSize, tiles));  
  return grid;
}

RuleEntropy
computeEntropy(
	       const std::vector<Rules> &rules
	       ) {
  RuleEntropy entropy;
  int count = 0;
  int lowest = std::numeric_limits<int>::max();
  for (const Rules &currentRule : rules) {
    for (const AllowedTilePlacement &allowedTiles : currentRule.allowedTiles) {
      count += allowedTiles.allowedTiles.size();
    }
    if (count < lowest) {
      lowest = count;
      entropy = {.rule = currentRule, .count = lowest};
    }
    count = 0;
  }

  return entropy;
}

int
pickRandomTileId(
		 int size,
		 std::vector<int> tilesID
		 ) {
  std::random_device rd;
  std::mt19937 generator(rd());
  std::uniform_int_distribution<> distribution(0, size - 1);
  int index = distribution(generator);
  return tilesID[index];
}

std::vector<int>
pickNeighbouringTilesId(
			RuleEntropy entropy
			) {
  std::vector<int> tilesID;
  for (auto &allowedTiles : entropy.rule.allowedTiles) {
    tilesID.push_back(pickRandomTileId(allowedTiles.tilesID.size(), allowedTiles.tilesID));
  }
  return tilesID;
}

png::image<png::rgb_pixel>
collapse(
	 std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>> grid,
	 std::vector<OccurencesCount> occurrences,
	 std::vector<Rules> rules,
	 int tileSize,
	 std::vector<png::image<png::rgb_pixel>> tiles) {
  
  int gridSize = grid.size();
  int outputWidth = gridSize * tileSize;
  int outputHeight = gridSize * tileSize;
  png::image<png::rgb_pixel> outputImage(outputWidth, outputHeight);

  // Pick middle index of the array
  int midX = gridSize / 2;
  int midY = gridSize / 2;

  // Pick lowest entropy tile
  RuleEntropy lowestEntropy = computeEntropy(rules);
  png::image<png::rgb_pixel> selectedTile = lowestEntropy.rule.tile;

  printf("entropy\n");

  // Place it at the middle of the output image
  for (int ty = 0; ty < tileSize; ty++) {
    for (int tx = 0; tx < tileSize; tx++) {
      outputImage[midY * tileSize + ty][midX * tileSize + tx] = selectedTile[ty][tx];
    }
  }

  // Keep track of the tiles to process (FIFO queue)
  // std::list<std::pair<int, int>> toProcess;
  // toProcess.push_back({midX, midY});

  printf("to process\n");

  // while (!toProcess.empty()) {
  //   auto [x, y] = toProcess.front();
  // toProcess.pop_front();


  // false, it needs to
  // place each neighbouring tiles
  // left, right, up, down order
  
  std::vector<int> neighbouringTileIds = pickNeighbouringTilesId(lowestEntropy);
  int i = 0;
  for (int id : neighbouringTileIds) {
    selectedTile = tiles[id];
    for (int ty = 0; ty < tileSize; ty++) {
      for (int tx = 0; tx < tileSize; tx++) {
	// since the order is left, right, up down
        if (i == 0) // on the left of the mid cell
                outputImage[(midY * tileSize) + ty][(midX * tileSize) - tileSize + tx] = selectedTile[ty][tx];

            if (i == 1) // on the right of the mid cell
                outputImage[(midY * tileSize) + ty][(midX * tileSize) + tileSize + tx] = selectedTile[ty][tx];

            if (i == 2) // above the mid cell
                outputImage[(midY * tileSize) - tileSize + ty][(midX * tileSize) + tx] = selectedTile[ty][tx];

            if (i == 3) // below the mid cell
                outputImage[(midY * tileSize) + tileSize + ty][(midX * tileSize) + tx] = selectedTile[ty][tx];
      }
    }
    i++;
  }

  // Save the output image
  outputImage.write("../output/collapsed.png");

  return outputImage;
}




// pick a random starting cell
// pick the tiles with the lowest entropy (the less possible neighbouring
// tiles)
// propagate to the neighbours the tile that have been locked in the cell
// each cell then remove from their list of possible tiles that do not match
// it in any way

// a cell is locked in if and only if it is the last possible choice
// if the tile that can be placed are 1 > pick a random to tiles to be placed
// (so remove the other one to match the rule)
