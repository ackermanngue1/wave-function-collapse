#ifndef WAVE_FUNCTION_COLLAPSE_H
#define WAVE_FUNCTION_COLLAPSE_H

#include <map>
#include <png++/image.hpp>
#include <png++/rgb_pixel.hpp>
#include <vector>

enum class DIRECTION { UP, RIGHT, DOWN, LEFT };

struct OccurencesCount {
  png::image<png::rgb_pixel> tile;
  int occurences;
};

struct AllowedTilePlacement {
  DIRECTION direction;
  std::vector<png::image<png::rgb_pixel>> allowedTiles;
  std::vector<int> tilesID;
};

struct Rules {
  png::image<png::rgb_pixel> tile;
  std::vector<AllowedTilePlacement> allowedTiles;
};

struct RuleEntropy {
  Rules rule;
  int count;
};


std::vector<png::image<png::rgb_pixel>>
extractTiles(const png::image<png::rgb_pixel> &baseImage, int tileSize);

std::vector<OccurencesCount>
countOccurrences(const png::image<png::rgb_pixel> &baseImage,
		 const std::vector<png::image<png::rgb_pixel>> &tiles);


bool checkTilesOverlappingCompability(
				      const png::image<png::rgb_pixel> &tileA,
				      const png::image<png::rgb_pixel> &tileB,
				      DIRECTION direction);

std::vector<Rules>
generateAdjacencyRules(
		       const std::vector<png::image<png::rgb_pixel>> &tiles
		       );

std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>>
createGrid(int gridSize,
           std::vector<png::image<png::rgb_pixel>> tiles);

png::image<png::rgb_pixel>
collapse(
	 std::vector<std::vector<std::vector<png::image<png::rgb_pixel>>>> grid,
	 std::vector<OccurencesCount> occurences,
	 std::vector<Rules> rules,
	 int tileSize,
	 std::vector<png::image<png::rgb_pixel>> tiles
);

RuleEntropy
computeEntropy(
	       const std::vector<Rules> &rules
);

std::vector<int>
pickNeighbouringTilesId(
		      RuleEntropy rule
);

int
pickRandomTileId(
		 int size,
		 std::vector<int> tilesID
);



#endif // WAVE_FUNCTION_COLLAPSE_H
